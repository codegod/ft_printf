/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ib.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/19 18:07:45 by ccuciurc          #+#    #+#             */
/*   Updated: 2017/01/19 18:07:47 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	setare(char *str, int base, uintmax_t val, int *p)
{
	char	tab[16] = "0123456789abcdef";

	while (val)
	{
		str[*p] = tab[val % base];
		(*p)++;
		val = val / base;
	}
}

char	*ft_ib(uintmax_t value, int base, int c)
{
	int		i;
	int		j;
	int 	aux;
	char	*str;

	i = 0;
	str = (char*)malloc(sizeof(char) * 69);
	setare(str, base, value, &i);
	if (value == 0)
		str[i++] = '0';
	j = 0;
	str[i] = '\0';
	if (c && value != 0)
	{
		str = ft_strcat(str, "x0");
		i += 2;
	}
	while (j < i / 2)
	{
		aux = str[j];
		str[j] = str[i - j - 1];
		str[i - j - 1] = aux;
		j++;
	}
	return (str);
}
