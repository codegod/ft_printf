/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/31 19:57:11 by ccuciurc          #+#    #+#             */
/*   Updated: 2016/10/31 20:14:55 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_print(long long n, int fd)
{
	if (n < 0)
	{
		n = -n;
		ft_putchar_fd('-', fd);
	}
	if (n < 10)
		ft_putchar_fd((n % 10) + '0', fd);
	else if (n >= 10)
	{
		ft_print(n / 10, fd);
		ft_print(n % 10, fd);
	}
}

void		ft_putnbr_fd(int nb, int fd)
{
	ft_print(nb, fd);
}
