/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/20 18:45:42 by ccuciurc          #+#    #+#             */
/*   Updated: 2016/10/31 18:35:59 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t	len3;
	size_t	len1;
	size_t	tmp;

	len3 = ft_strlen(little);
	len1 = ft_strlen(big);
	tmp = 0;
	if (!little)
		return (char*)(big);
	while (tmp + len3 <= len && tmp + len3 <= len1)
	{
		if (!ft_strncmp(big, little, len3))
			return ((char*)big);
		big++;
		tmp++;
	}
	return (NULL);
}
