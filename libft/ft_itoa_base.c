#include "libft.h"

static void rec(intmax_t n, int base, char *str, int *i)
{
	if (n == 0)
		return ;
	rec(n / base, base, str, i);
	str[(*i)++] = "0123456789abcdef"[n % base * (1 - 2 * (n < 0))];
}

char *ft_itoa_base(intmax_t value, int base)
{
	int i;
	char *str;

	str = (char*)malloc(sizeof(char) * 60);
	i = 0;
	if (value == 0)
		str[i++] = '0';
	if (value < -9223372036854775807)
		return (ft_strdup("-9223372036854775808"));
	if (value < 0)
	{
		str[i++] = '-';
		value = -value;
	}
	rec(value, base, str, &i);
	str[i] = '\0';
	return (str);
}