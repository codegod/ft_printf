/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/06 18:16:11 by ccuciurc          #+#    #+#             */
/*   Updated: 2016/11/01 19:51:37 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

#include "libft.h"
#include <string.h>

void	*ft_memmove(void *s1, const void *s2, size_t n)
{
	const char	*src;
	char		*dest;

	src = s2;
	dest = s1;
	if (dest <= src || src >= (dest + n))
	{
		while (n)
		{
			*dest++ = *src++;
			n--;
		}
	}
	else
	{
		src = src + n - 1;
		dest = dest + n - 1;
		while (n)
		{
			*dest-- = *src--;
			n--;
		}
	}
	return (s1);
}
