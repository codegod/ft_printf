/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/31 19:34:05 by ccuciurc          #+#    #+#             */
/*   Updated: 2016/10/31 20:14:15 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_print(long long n)
{
	if (n < 0)
	{
		n = -n;
		ft_putchar('-');
	}
	if (n < 10)
		ft_putchar((n % 10) + '0');
	else if (n >= 10)
	{
		ft_print(n / 10);
		ft_print(n % 10);
	}
}

void		ft_putnbr(int nb)
{
	ft_print(nb);
}
