/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/31 19:14:37 by ccuciurc          #+#    #+#             */
/*   Updated: 2016/12/09 19:12:44 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*newstr;
	size_t	length;

	newstr = NULL;
	if (s1 && s2)
	{
		length = ft_strlen(s1) + ft_strlen(s2);
		newstr = (char *)malloc(sizeof(char) * (length + 1));
		if (newstr)
		{
			ft_strcpy(newstr, s1);
			ft_strcat(newstr, s2);
		}
	}
	return (newstr);
}
