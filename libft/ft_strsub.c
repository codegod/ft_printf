/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/31 19:14:02 by ccuciurc          #+#    #+#             */
/*   Updated: 2016/10/31 19:14:27 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	size_t		length;
	char		*str;

	if (s == NULL)
		return (NULL);
	length = ft_strlen(s);
	str = NULL;
	if ((start + len) <= length)
	{
		str = (char *)malloc(sizeof(char) * (len + 1));
		if (str)
		{
			str = ft_strncpy(str, s + start, len);
			str[len] = '\0';
		}
	}
	return (str);
}
