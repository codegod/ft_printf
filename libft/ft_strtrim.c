/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/31 19:15:27 by ccuciurc          #+#    #+#             */
/*   Updated: 2016/10/31 20:47:16 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int		a(int c)
{
	return (c == ' ' || c == '\t' || c == '\n' || c == '\v' || c == '\f'
			|| c == '\r');
}

char			*ft_strtrim(char const *s)
{
	size_t	start;
	size_t	end;
	char	*new;

	if (s)
	{
		start = 0;
		end = ft_strlen(s);
		while (a(s[start]))
			start++;
		while (a(s[end - 1]))
			end--;
		if (end < start)
			end = start;
		new = ft_strnew(end - start);
		if (new == NULL)
			return (NULL);
		return (ft_strncpy(new, s + start, end - start));
	}
	return (NULL);
}
