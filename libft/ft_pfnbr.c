#include "libft.h"

static void	ft_print(long long n, int *c)
{
    if (n >= 10)
    {
        ft_print(n / 10, c);
        ft_print(n % 10, c);
    }
    else if (n < 10)
    {
        ft_putchar((n % 10) + '0');
        *c = *c + 1;
    }
}

#include <stdio.h>

int			ft_pfnbr(int nb, int b, int c)
{
    long long	n;
    int		nr;

    printf("\nc: %d\n", c);
    n = nb;
    nr = 0;
    if (n < 0)
    {
        nr++;
        c--;
        n = -n;
        ft_putchar('-');
    }
    else if (b)
    {
        nr++;
        c--;
        ft_putchar('+');
    }
    while (c && c > 0)
    {
        ft_putchar('0');
        c--;
        nr++;
    }
    ft_print(n, &nr);
    return (nr);
}