/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstremoveif.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/09 17:44:32 by ccuciurc          #+#    #+#             */
/*   Updated: 2016/12/09 20:06:12 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_list_remove_if(t_list **begin_list, size_t data_ref, int (*cmp)())
{
	t_list		*free_me;

	if (*begin_list)
	{
		if (cmp((*begin_list)->content_size, data_ref))
		{
			free_me = *begin_list;
			*begin_list = (*begin_list)->next;
			ft_strclr(free_me->content);
			free(free_me);
			ft_list_remove_if(begin_list, data_ref, cmp);
		}
		else
			ft_list_remove_if(&(*begin_list)->next, data_ref, cmp);
	}
}
