/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pnbr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/19 18:08:23 by ccuciurc          #+#    #+#             */
/*   Updated: 2017/01/19 18:08:25 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void rec(intmax_t n, int base, char *str, int *i)
{
    if (n == 0)
        return ;
    rec(n / base, base, str, i);
    str[(*i)++] = "0123456789abcdef"[n % base * (1 - 2 * (n < 0))];
}

char *ft_itoa_base1(intmax_t value, int base)
{
    int i;
    char *str;

    str = (char*)malloc(sizeof(char) * 60);
    i = 0;
    if (value == 0)
        str[i++] = '0';
    if (value < -9223372036854775807)
    {
        free(str);
        return (ft_strdup("9223372036854775808"));
    }
    if (value < 0)
        value = -value;
    rec(value, base, str, &i);
    str[i] = '\0';
    return (str);
}

void	ft_print(intmax_t n, int *c)
{
    if (n >= 10)
    {
        ft_print(n / 10, c);
        ft_print(n % 10, c);
    }
    else if (n < 10)
    {
        ft_putchar((n % 10) + '0');
        *c = *c + 1;
    }
}

int			ft_pnbr(intmax_t nb, int b, int c)
{
    intmax_t	n;
    int		nr;
    char *s;

    n = nb;
    nr = 0;
    if (n < 0)
    {
        nr++;
        n = -n;
        ft_putchar('-');
    }
    else if (b)
    {
        nr++;
        ft_putchar('+');
    }
    while (c != 0 && c > 0)
    {
        ft_putchar('0');
        c--;
    }
    s = ft_itoa_base1(n, 10);
    nr += ft_pst(s);
    free(s);
    return (nr);
}