/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ibup.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/19 18:07:55 by ccuciurc          #+#    #+#             */
/*   Updated: 2017/01/19 18:07:57 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	setareup(char *str, int base, uintmax_t val, int *p)
{
	char	tab[16] = "0123456789ABCDEF";

	while (val)
	{
		str[*p] = tab[val % base];
		(*p)++;
		val = val / base;
	}
}

char	*ft_ibup(uintmax_t value, int base, int c)
{
	int		i;
	int		j;
	int 	aux;
	char	*str;

	i = 0;
	str = (char*)malloc(sizeof(char) * 69);
	setareup(str, base, value, &i);
	if (value == 0)
		str[i++] = '0';
	j = 0;
	str[i] = '\0';
	if (c && value != 0)
	{
		str = ft_strcat(str, "X0");
		i += 2;
	}
	while (j < i / 2)
	{
		aux = str[j];
		str[j] = str[i - j - 1];
		str[i - j - 1] = aux;
		j++;
	}
	return (str);
}
