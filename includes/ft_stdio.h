/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 16:19:51 by ccuciurc          #+#    #+#             */
/*   Updated: 2017/01/19 17:33:56 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STDIO_H
# define FT_STDIO_H

#include <libft.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>

typedef struct 	s_flag
{
	int		m;
	int		p;
	int		s;
	int		d;
	int		z;
}				t_flag;

typedef struct	s_printf
{
	t_flag		*f;
	int			w;
	int			p;
	char		*l;
	char		s;
}				t_printf;

int		ft_printf(char*, ...);

void	ft_pinit(t_printf **v);

int		ft_sw(t_printf *v, va_list ap);
int		ft_sw1(t_printf *v, va_list ap);
int		ft_sw2(t_printf *v, va_list ap);
int		ft_sw3(t_printf *v, va_list ap);
int		ft_sw4(t_printf *v, va_list ap);
int		ft_sw5(t_printf *v, va_list ap);
int		ft_sw6(t_printf *v, va_list ap);
int		ft_sw7(t_printf *v, va_list ap);
int		ft_fl(char);
int		ft_wd(char);
int		ft_isl(char);
int		ft_nrl(char*);
int		ft_prints(t_printf*, char*);
int		ft_pd(t_printf*, intmax_t);
int		ft_pp(t_printf*, uintmax_t, long int);
int		ft_ppt(t_printf*, uintmax_t);
int		ft_ph(t_printf*, uintmax_t, int);
int		ft_pch(t_printf*, char);
int		ft_vrf(int, int, int, int, t_printf*);
int		ft_psp(t_printf *v, wchar_t*);
int		ft_pspch(t_printf *v, wchar_t);
int		ft_wstrlen(wchar_t *str);
size_t  calc_wstrlen(wchar_t *str, int precision, size_t i);
int 	ft_putwstr(wchar_t*, size_t);
void	ft_which(t_flag**, char);
int		ft_hasfl(t_flag *f);
void	ft_pinit(t_printf **v);
int		ft_is_bigger(char*, char*, int);
wchar_t *ft_wstrnew(char*);
int   	ft_wstrl(wchar_t *wstr, size_t len);
void	ft_uninit(t_printf **v);
int		ft_sw1c(t_printf *v, va_list ap);
int     ft_pspc(t_printf *v, wchar_t *s);
int 	ft_printsc(t_printf *v, char *s);
void    ft_det_flags_width(char **c, va_list ap, t_printf **v);
void    ft_det_precision(char **c, va_list ap, t_printf **v);
void    ft_det_length(char **c, t_printf **v);
int     ft_pd_ver1(t_printf *v, intmax_t n, int ln);
int     ft_pd_ver2(t_printf *v, intmax_t n, int ln);
int     ft_pd_ver3(t_printf *v, intmax_t n, int ln);
int     ft_pd_ver4(t_printf *v, intmax_t n, int ln);
int     ft_ph_ver1(t_printf *v, uintmax_t n, int cn, int ln);
int     ft_ph_ver2(t_printf *v, uintmax_t n, int cn, int ln, int ln1);
int     ft_ph_ver3(t_printf *v, uintmax_t n, int cn, int ln);
int     ft_ph_ver4(t_printf *v, uintmax_t n, int cn, int ln);
int     ft_ph_ver5(t_printf *v, uintmax_t n, int cn, int ln);
int     ft_ph_ver6(t_printf *v, uintmax_t n, int cn, int ln);
int     ft_ph_ver7(t_printf *v, int ln);
int     ft_pnbb(uintmax_t, int, int, int);
int		ft_pnbb1(uintmax_t n, int p, int i, int p1);
int     ft_ppt_ver1(t_printf *v, uintmax_t n, int ln);
int     ft_ppt_ver2(t_printf *v, uintmax_t n, int ln);
int     ft_ppt_ver3(t_printf *v, uintmax_t n, int ln);
int     ft_ppt_ver4(t_printf *v, uintmax_t n, int ln);
int     ft_ppt_ver5(t_printf *v, uintmax_t n, int ln);
int		ft_pp_ver1(t_printf *v, uintmax_t n, int ln, char *s);
int		ft_pp_ver2(t_printf *v, uintmax_t n, int ln, char *s);
int		ft_pp_ver3(t_printf *v, uintmax_t n, int ln, char *s);
int		ft_pp_ver4(t_printf *v, uintmax_t n, int ln, char *s);
int		ft_pp_ver41(t_printf *v, int ln);

#endif