/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bool_func.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 16:19:34 by ccuciurc          #+#    #+#             */
/*   Updated: 2017/01/17 16:19:39 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stdio.h"

int	ft_fl(char c)
{
	return (c == '-' || c == '+' || c == ' ' || c == '#' || c == '0');
}

int	ft_wd(char c)
{
	return (ft_isdigit(c) || c == '*');
}

int	ft_isl(char c)
{
	return (c == 'h' || c == 'l' || c == 'j' || c == 'z' || c == 't'
			|| c == 'L');
}

int	ft_nrl(char *s)
{
    if (*s == *(s + 1))
        return (2);
    if (*s)
        return (1);
    return (0);
}