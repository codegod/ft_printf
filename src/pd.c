#include "ft_stdio.h"

int     ft_pd(t_printf *v, intmax_t n)
{
    int nr;
    int ln;
    char *s;

    s = ft_itoa(n);
    ln = (int)ft_strlen(s);
    free(s);
    nr = 0;
    if (v->f->s && !v->f->p && !v->f->m && n >= 0)
        nr += ft_pst(" ");
    if (v->f->m)
        nr += ft_pd_ver1(v, n, ln);
    else if (v->f->p && !v->f->z)
        nr += ft_pd_ver2(v, n, ln);
    else if (v->f->p && v->f->z)
    {
        if (n < 0)
            ln--;
        nr += ft_pfnbr(n, 1, v->p > v->w ? v->p - ln : v->w - ln);
    }
    else if (v->f->z)
        nr = ft_pd_ver3(v, n, ln);
    else
        nr = ft_pd_ver4(v, n, ln);
    return (nr);
}

int ft_pd_ver1(t_printf *v, intmax_t n, int ln)
{
    int nr;

    nr = 0;
    if (v->f->s && !v->f->p && !v->f->m && n >= 0)
        nr++;
    if (v->f->p)
        nr = ft_pnbr(n, 1, v->p - ln);
    else
        nr = ft_pnbr(n, 0, v->p - ln);
    v->p != -1 ? nr += v->p - ln : 0;
    while (nr++ < v->w)
        ft_putchar(' ');
    nr--;
    return (nr);
}

int ft_pd_ver2(t_printf *v, intmax_t n, int ln)
{
    int nr;

    nr = 0;
    if (v->f->s && !v->f->p && !v->f->m && n >= 0)
        nr++;
    v->p != -1 ? nr += v->p - ln : 0;
    n >= 0 ? nr++ : 0;
    while (nr++ < (v->w - ln))
        ft_putchar(' ');
    n >= 0 ? nr -= 2 : nr--;
    nr += ft_pnbr(n, 1, v->p - ln);
    return (nr);
}

int ft_pd_ver3(t_printf *v, intmax_t n, int ln)
{
    int nr;

    nr = 0;
    if (v->f->s && !v->f->p && !v->f->m && n >= 0)
        nr++;
    if (n < 0)
        nr += ft_pfnbr(n, 0, (v->p > v->w ? v->p - ln : v->w - ln) + 1);
    else
    {
        if (v->p <= 0)
            while (nr++ < (v->w - ln))
                ft_putchar('0');
        else
            while (nr++ < (v->w - v->p))
                ft_putchar(' ');
        nr--;
        nr += ft_pfnbr(n, 0, v->p - ln);
    }
    return (nr);
}

int ft_pd_ver4(t_printf *v, intmax_t n, int ln)
{
    int nr;

    nr = 0;
    if (v->f->s && !v->f->p && !v->f->m && n >= 0)
        nr++;
    if ((v->p == 0) && (n == 0))
        if (v->w > 0)
            v->w++;
    n < 0 ? ln-- : 0;
    v->p != -1 && v->p > ln ? nr += v->p - ln : 0;
    if (n < 0)
        v->w--;
    while (nr++ < (v->w - ln))
        ft_putchar(' ');
    nr--;
    if (n == 0 && v->p == 0)
        ft_pst("");
    else
        nr += ft_pnbr(n, 0, v->p - ln);
    return (nr);
}