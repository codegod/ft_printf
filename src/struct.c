#include "ft_stdio.h"

void	ft_pinit(t_printf **v)
{
	*v = (t_printf*)malloc(sizeof(t_printf));
	(*v)->f = (t_flag*)malloc(sizeof(t_flag));
	(*v)->f->m = 0;
	(*v)->f->p = 0;
	(*v)->f->s = 0;
	(*v)->f->d = 0;
	(*v)->f->z = 0;
	(*v)->w = -1;
	(*v)->p = -1;
	(*v)->s = 'n';
	(*v)->l = ft_strdup("n");
}

void	ft_which(t_flag **f, char c)
{
	if (c == '-')
		(*f)->m = 1;
	if (c == '+')
		(*f)->p = 1;
	if (c == ' ')
		(*f)->s = 1;
	if (c == '#')
		(*f)->d = 1;
	if (c == '0')
		(*f)->z = 1;
}

void	ft_uninit(t_printf **v)
{
	free((*v)->l);
	free((*v)->f);
	free(*v);
	v = NULL;
}