/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pointer.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 16:20:13 by ccuciurc          #+#    #+#             */
/*   Updated: 2017/01/19 17:41:30 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stdio.h"

int		ft_ppt(t_printf *v, uintmax_t n)
{
	int	nr;
	char *s;
	int ln;
	int ln1;

	s = ft_ib(n, 16, 0);
	ln = (int)ft_strlen(s);
	free(s);
	s = ft_ib(n, 16, 1);
	ln1 = (int)ft_strlen(s);
	free(s);
	v->f->d = 1;
	if (v->f->p)
		nr = ft_ppt_ver1(v, n, ln);
	else if (v->f->m)
        nr = ft_ppt_ver2(v, n, ln);
	else if (v->f->d)
        nr = ft_ppt_ver3(v, n, ln);
	else if (v->f->d && v->p == -1)
        nr = ft_ppt_ver4(v, n, ln1);
	else
        nr = ft_ppt_ver5(v, n, ln1);
	return (nr);
}

int ft_ppt_ver1(t_printf *v, uintmax_t n, int ln)
{
    int nr;

    nr = 0;
    while (++nr < (v->w - ln))
        ft_putchar(' ');
    nr--;
    ft_putstr("0x");
    nr += 2;
    nr += ft_pnbb1(n, v->p - ln, 0, v->p);
    return (nr);
}

int ft_ppt_ver2(t_printf *v, uintmax_t n, int ln)
{
    int nr;

    nr = 0;
    ft_putstr("0x");
    nr += 2;
    nr += ft_pnbb1(n, v->p - ln, 0, v->p);
    while (nr++ < v->w)
        ft_putchar(' ');
    nr--;
    return (nr);
}

int ft_ppt_ver3(t_printf *v, uintmax_t n, int ln)
{
    int nr;

    nr = 0;
    while (!v->f->z && nr++ < (v->w - ln) - 2)
        if (!v->f->z)
            ft_putchar(' ');
    ft_putstr("0x");
    nr += 2;
    while (v->f->z && nr++ < (v->w - ln))
        if (v->f->z)
            ft_putchar('0');
    nr--;
    nr += ft_pnbb1(n, v->p - ln, 0, v->p);
    return (nr);
}