/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 16:24:02 by ccuciurc          #+#    #+#             */
/*   Updated: 2017/01/17 17:03:31 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

#include <ft_stdio.h>

#include <limits.h>

int main()
{
//	char	*s = "Salut.";
//	int	i = -12345;
//	int	nr = 23;
//	int	*n = &nr;

	// printf("%i\n", printf("String: |%-10s| Costea\n", s));
	// printf("%i\n", ft_printf("String: |%-10s| Costea\n", s));
	// printf("%i\n", printf("String: |%10s| Costea\n", s));
	// printf("%i\n", ft_printf("String: |%10s| Costea\n", s));
	// printf("%i\n", printf("String: |%s| Costea\n", s));
	// printf("%i\n", ft_printf("String: |%s| Costea\n", s));
	// printf("%i\n", printf("String: |%12.4s| Costea\n", s));
	// printf("%i\n", ft_printf("String: |%12.4s| Costea\n", s));
	// printf("%i\n", printf("String: |%%| %s Costea\n", s));
	// printf("%i\n", ft_printf("String: |%%| %s Costea\n", s));
	// printf("%i\n", printf("String: |%-10.1s| Costea\n", s));
	// printf("%i\n", ft_printf("String: |%-10.1s| Costea\n", s));
	// printf("%i\n", printf("String: |%-*.*s| Costea\n", 12, 3, s));
	// printf("%i\n", ft_printf("String: |%-*.*s| Costea\n", 12, 3, s));
	// printf("%i\n", printf("String: |%5%|\n"));
	// printf("%i\n", ft_printf("String: |%5%|\n"));
	// printf("%i\n", printf("String: |%-5%|\n"));
	// printf("%i\n", ft_printf("String: |%-5%|\n"));
	//printf("%i\n", printf("String: |%   %|\n", "Salut"));
	//printf("%i\n", ft_printf("String: |%   %|\n", "Salut"));
	//printf("%i\n", printf("String: |%.0%| Costea\n"));
	//printf("%i\n", ft_printf("String: |%.0%| Costea\n"));
	// printf("%i\n", printf("String: |%i| Costea\n", i));
	// printf("%i\n", ft_printf("String: |%i| Costea\n", i));
	// printf("%i\n", printf("String: |%+i| Costea\n", -i));
	// printf("%i\n", ft_printf("String: |%+i| Costea\n", -i));
	// printf("%i\n", printf("String: |%+i| Costea\n", nr));
	// printf("%i\n", ft_printf("String: |%+i| Costea\n", nr));
	// printf("%i\n", printf("-String: |%-10i| Costea\n", nr));
	// printf("%i\n", ft_printf("-String: |%-10i| Costea\n", nr));
	// printf("%i\n", printf("+String: |%+10i| Costea\n", nr));
	// printf("%i\n", ft_printf("+String: |%+10i| Costea\n", nr));
	// printf("%i\n", printf("String: |%10i| Costea\n", nr));
	// printf("%i\n", ft_printf("String: |%10i| Costea\n", nr));
	// printf("%i\n", printf("String: |%010i| Costea\n", nr));
	// printf("%i\n", ft_printf("String: |%010i| Costea\n", nr));
	// printf("%i\n", printf("String: |% 10i| Costea\n", -nr));
	// printf("%i\n", ft_printf("String: |% 10i| Costea\n", -nr));
	// printf("Precision:\n");
	// printf("%i\n", printf("String: |%-.4i| Costea\n", nr));
	// printf("%i\n", ft_printf("String: |%-.4i| Costea\n", nr));
	// printf("%i\n", printf("String: |%+.4i| Costea\n", nr));
	// printf("%i\n", ft_printf("String: |%+.4i| Costea\n", nr));
	// printf("%i\n", printf("String: |%10.4i| Costea\n", nr));
	// printf("%i\n", ft_printf("String: |%10.4i| Costea\n", nr));
	// printf("%i\n", printf("String: |%.4i| Costea\n", nr));
	// printf("%i\n", ft_printf("String: |%.4i| Costea\n", nr));
	
	// printf("%i\n", printf("String: |%10p| Costea\n", n));
	// printf("%i\n", ft_printf("String: |%10p| Costea\n", n));
	
	// printf("%i\n", printf("String: |%10c| Costea\n", 'c'));
	// printf("%i\n", ft_printf("String: |%10c| Costea\n", 'c'));
	// printf("%i\n", printf("String: |%-10c| Costea\n", 'c'));
	// printf("%i\n", ft_printf("String: |%-10c| Costea\n", 'c'));
	// printf("%i\n", printf("String: |%c| Costea\n", 'c'));
	// printf("%i\n", ft_printf("String: |%c| Costea\n", 'c'));


	// printf("%i\n", printf("String: |%10.2o| Costea\n", 733));
	// printf("%i\n", ft_printf("String: |%10.2o| Costea\n", 733));

	// printf("%i\n", printf("String: |%10.2u| Costea\n", 733));
	// printf("%i\n", ft_printf("String: |%10.2u| Costea\n", 733));

	// printf("%i\n", printf("String: |%10.2x| Costea\n", 11));
	// printf("%i\n", ft_printf("String: |%10.2x| Costea\n", 11));
	// printf("%i\n", printf("String: |%-10.2x| Costea\n", 11));
	// printf("%i\n", ft_printf("String: |%-10.2x| Costea\n", 11));

	// printf("%i\n", printf("String: |%10.2X| Costea\n", 11));
	// printf("%i\n", ft_printf("String: |%10.2X| Costea\n", 11));
	// //printf("%i\n", printf("String: |%+10.2X| Costea\n", 11));
	// //printf("%i\n", ft_printf("String: |%+10.2X| Costea\n", 11));
	// printf("%i\n", printf("String: |%-10.2X| Costea\n", 11));
	// printf("%i\n", ft_printf("String: |%-10.2X| Costea\n", 11));

	// printf("Length:\n");
	// printf("%i\n", printf("String: |%hhi| Costea\n", 'c'));
	// printf("%i\n", ft_printf("String: |%hhi| Costea\n", 'c'));

	// printf("Unicode\n");
	// ft_printf("%10!\n");

//	 printf("%s\n", "ƺ");
//	 ft_printf("%ls\n", L"ƺ");
//	 ft_printf("%llX\n", 4294967296);
//	 ft_printf("%zd\n", 4294967296);
//	 ft_printf("%10x\n", 42);
//	 ft_printf("%-10x|\n", 42);
//	 printf("\n%i\n", ft_printf("%#x\n", 42));
//	 printf("\n%i\n", ft_printf("@moulitest: %#.x %#.0x\n", 0, 0));
//	 printf("\n%i\n", printf("@moulitest: %#.x %#.0x\n", 12, 11));
//	 printf("\n%i\n", printf("@moulitest: %s\n", NULL));
//	 printf("\n%i\n", ft_printf("@moulitest: %s\n", NULL));
//
//
//	printf("\n%i\n", ft_printf("|%-20.10o|", 2500));
//	printf("\n%i\n", ft_printf("|%0+5d|", 42));
//	printf("\n%i\n", ft_printf("%lld", -9223372036854775808));
//    printf("\n%i\n", printf("%lld", -9223372036854775808));
//    printf("\n%i\n", ft_printf("|%.2d|", 4242));
//    printf("\n%i\n", ft_printf("|%-+10.5d|", 4242));
//    printf("\n%i\n", ft_printf("|%03.2d|", 1));
//    printf("\n%i\n", ft_printf("|%03.2o|", 1));
//    printf("\n%i\n", ft_printf("|%05o|", 42));
//    printf("\n%i\n", ft_printf("%+u", "4294967295"));
//    printf("\n%i\n", ft_printf("|@moulitest: %5.d %5.0d|", 0, 0));
//    printf("\n%i\n", printf("|@moulitest: %5.d %5.0d|", 0, 0));
//    printf("\n%i\n", ft_printf("|%ll#x|", 9223372036854775807));
//    printf("\n%i\n", ft_printf("%s  ", "this is a string"));
    printf("\n%i\n", printf("% d", 9999));
    printf("\n%i\n", ft_printf("% d", 9999));
	return (0);
}
