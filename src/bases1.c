#include "ft_stdio.h"

int		ft_pp_ver3(t_printf *v, uintmax_t n, int ln, char *s)
{
    int nr;

    nr = 0;
    nr++;
    if (v->p > ln)
    {
        while (nr++ < (v->w - ln - (v->p - ln)))
            ft_putchar(' ');
        nr--;
        while (nr++ < (v->p - ln))
            ft_putchar('0');
    }
    else
        while (nr++ < (v->w - ln))
            ft_putchar(' ');
    ft_putchar('0');
    nr--;
    if (n != 0)
        nr += ft_pst(s);
    return (nr);
}

int		ft_pp_ver4(t_printf *v, uintmax_t n, int ln, char *s)
{
    int nr;

    nr = 0;
    if (v->p > ln)
    {
        nr = ft_pp_ver41(v, ln);
    }
    else
        while (nr++ < (v->w - ln))
            if (v->f->z)
                ft_putchar('0');
            else
                ft_putchar(' ');
    nr--;
    if ((n == 0 && v->p != 0) || (n != 0))
        nr += ft_pst(s);
    return (nr);
}

int ft_pp_ver41(t_printf *v, int ln)
{
    int nr;

    nr = 0;
    while (nr++ < (v->w - ln - (v->p - ln)))
        ft_putchar(' ');
    if (v->w > v->p)
    {
        while (nr++ < (v->w - ln + (v->p - ln)) - 1)
            ft_putchar('0');
        nr--;
    }
    else
    {
        nr--;
        while (nr++ < (v->p - ln))
            ft_putchar('0');

    }
    return (nr);
}