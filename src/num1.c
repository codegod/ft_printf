#include "ft_stdio.h"

int ft_ph_ver5(t_printf *v, uintmax_t n, int cn, int ln)
{
    int nr;

    nr = 0;
    while (nr++ < (v->w - ln))
        ft_putchar(' ');
    nr--;
    nr += ft_pnbb(n, v->p - ln, 1, cn);
    return (nr);
}

int ft_ph_ver6(t_printf *v, uintmax_t n, int cn, int ln)
{
    int nr;

    nr = 0;
    while (nr++ < (v->w - (ln + (v->p != -1 && v->p > ln ? v->p - ln : 0))))
        ft_putchar(' ');
    nr--;
    nr += ft_pnbb(n, v->p - ln, 0, cn);
    return (nr);
}

int ft_ph_ver7(t_printf *v, int ln)
{
    int nr;

    nr = 0;
    nr--;
    while (nr++ < (v->w - ln))
        ft_putchar(' ');
    return (nr);
}

int     ft_pnbb(uintmax_t n, int p, int i, int cn)
{
    int nr;
    char *s;
    char *s1;

    nr = 0;
    if (i / 10)
        ft_putchar('+');
    while (nr++ < p)
        ft_putchar('0');
    nr--;
    i / 10 ? nr++ : 3;
    s = ft_ibup(n, 16, i % 10);
    s1 = ft_ib(n, 16, i % 10);
    if (cn)
        nr += ft_pst(s);
    else
        nr += ft_pst(s1);
    free(s);
    free(s1);
    return (nr);
}