/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vrf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 16:20:35 by ccuciurc          #+#    #+#             */
/*   Updated: 2017/01/17 16:20:37 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stdio.h"

int ft_val(char *s, size_t n)
{
    if (ft_strncmp("hh", s, n) == 0)
        return (1);
    if (ft_strncmp("h", s, n) == 0)
        return (2);
    if (ft_strncmp("l", s, n) == 0)
        return (3);
    if (ft_strncmp("ll", s, n) == 0)
        return (4);
    if (ft_strncmp("j", s, n) == 0)
        return (5);
    if (ft_strncmp("z", s, n) == 0)
        return (6);
    if (ft_strncmp("t", s, n) == 0)
        return (7);
    if (ft_strncmp("L", s, n) == 0)
        return (8);
    return (0);
}

int     ft_is_bigger(char *s1, char *s2, int nr)
{
    if (*s1 == 'n')
        return (1);
    if (ft_val(s1, ft_strlen(s1)) < ft_val(s2, (size_t)nr))
        return (1);
    return (0);
}