#include "ft_stdio.h"

int ft_ppt_ver4(t_printf *v, uintmax_t n, int ln)
{
    int nr;

    nr = 0;
    while (nr++ < (v->w - ln))
        ft_putchar(' ');
    nr--;
    ft_putstr("0x");
    nr += 2;
    nr += ft_pnbb1(n, v->p - ln, 1, v->p);
    return (nr);
}

int ft_ppt_ver5(t_printf *v, uintmax_t n, int ln)
{
    int nr;

    nr = 0;
    while (nr++ < (v->w - (ln + (v->p != -1 && v->p > ln ? v->p - ln : 0))))
        ft_putchar(' ');
    nr--;
    ft_putstr("0x");
    nr += 2;
    nr += ft_pnbb1(n, v->p - ln, 0, v->p);
    return (nr);
}

int		ft_pnbb1(uintmax_t n, int p, int i, int p1)
{
	int	nr;
	char *s;

	nr = 0;
	if (i / 10)
		ft_putchar('+');
	while (nr++ < p)
		ft_putchar('0');
	nr--;
	i / 10 ? nr++ : 3;
	s = ft_ib(n, 16, i % 10);
	if (!(n == 0 && p <= 0 && p1 != -1))
		nr += ft_pst(s);
	free(s);
	return (nr);
}