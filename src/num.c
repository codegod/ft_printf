/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   num.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 16:19:58 by ccuciurc          #+#    #+#             */
/*   Updated: 2017/01/19 18:04:56 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stdio.h"

int     ft_ph(t_printf *v, uintmax_t n, int cn)
{
    char *s;
    int ln[3];

    s = ft_ib(n, 16, 0);
    ln[0] = (int)ft_strlen(s);
    free(s);
    s = ft_ib(n, 16, 1);
    ln[1] = (int)ft_strlen(s);
    free(s);
    ln[2] = 0;
    if (v->f->p)
        ln[2] = ft_ph_ver1(v, n, cn, ln[0]);
    else if (v->f->m)
        ln[2] = ft_ph_ver2(v, n, cn, ln[0], ln[1]);
    else if (v->f->z && !v->f->d && n != 0)
        ln[2] = ft_ph_ver3(v, n, cn, ln[0]);
    else if (v->f->d && n != 0)
        ln[2] = ft_ph_ver4(v, n, cn, ln[0]);
    else if (v->f->d && v->p == -1)
        ln[2] = ft_ph_ver5(v, n, cn, ln[1]);
    else if (n == 0 && v->p == 0)
        ln[2] = ft_ph_ver7(v, ln[0]);
    else
        ln[2] = ft_ph_ver6(v, n, cn, ln[0]);
    return (ln[2]);
}

int ft_ph_ver1(t_printf *v, uintmax_t n, int cn, int ln)
{
    int nr;

    nr = 0;
    while (++nr < (v->w - ln))
        ft_putchar(' ');
    nr--;
    nr += ft_pnbb(n, v->p - ln, 0, cn);
    return (nr);
}

int ft_ph_ver2(t_printf *v, uintmax_t n, int cn, int ln, int ln1)
{
    int nr;

    nr = 0;
    if (v->f->d)
        nr = ft_pnbb(n, v->p - ln1, 1, cn);
    else
        nr = ft_pnbb(n, v->p - ln, 0, cn);
    while (nr++ < v->w)
        ft_putchar(' ');
    nr--;
    return (nr);
}

int ft_ph_ver3(t_printf *v, uintmax_t n, int cn, int ln)
{
    int nr;

    nr = 0;
    while (nr++ < (v->w - ln))
        ft_putchar('0');
    nr--;
    nr += ft_pnbb(n, v->p - ln, 0, cn);
    return (nr);
}

int ft_ph_ver4(t_printf *v, uintmax_t n, int cn, int ln)
{
    int nr;

    nr = 0;
    while (!v->f->z && nr++ < (v->w - ln) - 2)
        if (!v->f->z)
            ft_putchar(' ');
    cn ? ft_putstr("0X") : ft_putstr("0x");
    nr += 2;
    while (v->f->z && nr++ < (v->w - ln))
        if (v->f->z)
            ft_putchar('0');
    nr--;
    nr += ft_pnbb(n, v->p - ln, 0, cn);
    return (nr);
}