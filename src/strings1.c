#include "ft_stdio.h"

int		ft_pspch(t_printf *v, wchar_t c)
{
    int	nr;

    nr = 0;
    if (v->f->m)
    {
        nr = ft_putchar_fdw(c, 1);
        while (++nr < v->w)
            ft_putchar(' ');
    }
    else
    {
        while (++nr < v->w)
            if (v->f->z)
                ft_putchar('0');
            else
                ft_putchar(' ');
        nr = ft_putchar_fdw(c, 1);
    }
    return (nr);
}

int		ft_psp(t_printf *v, wchar_t *s)
{
	int		n;

	n = 0;
	if (s == NULL)
	{
		s = ft_wstrnew("(null)");
	}
	if (v->f->m && !v->f->z)
	{
		if (v->p != -1)
			n = ft_putwstr(s, (size_t)v->p);
		else
			n = ft_putwstr(s, (size_t)ft_wstrlen(s));
		while (n++ < v->w)
			ft_putchar(' ');
		n--;
	}
	else
		n = ft_pspc(v, s);
	if (s == NULL)
		free(s);
	return (n);
}

int ft_pspc(t_printf *v, wchar_t *s)
{
	int		n;

	n = 0;
	while (n < (v->w - ft_wstrl(s, v->p)))
	{
		if (v->f->z)
			ft_putchar('0');
		else
			ft_putchar(' ');
		n++;
	}
	if (v->p != -1)
		n += ft_putwstr(s, (size_t)v->p);
	else
		n += ft_putwstr(s, (size_t)ft_wstrlen(s));
	return (n);
}