#include "ft_stdio.h"

int		ft_pp(t_printf *v, uintmax_t n, long int b)
{
    int	nr;
    int ln;
    char *s;

    s = ft_ib(n, b, b == 16);
    nr = 0;
    ln = (int)ft_strlen(s);
    if (v->w != -1 && v->w && !v->p && n == 0)
        v->w++;
    if (v->f->p)
        nr = ft_pp_ver1(v, n, ln, s);
    else if (v->f->m)
        nr = ft_pp_ver2(v, n, ln, s);
    else if (v->f->d)
        nr = ft_pp_ver3(v, n, ln, s);
    else
        nr = ft_pp_ver4(v, n, ln, s);
    free(s);
    return (nr);
}

int		ft_pp_ver1(t_printf *v, uintmax_t n, int ln, char *s)
{
    int nr;

    nr = 0;
    if (v->p > ln)
    {
        while (nr++ < (v->w - ln - (v->p - ln)))
            ft_putchar(' ');
        nr--;
        while (nr++ < (v->p - ln))
            ft_putchar('0');
    }
    else {
        while (nr++ < (v->w - ln))
            ft_putchar(' ');
        nr--;
    }
    while (nr++ < (v->p - ln))
        ft_putchar('0');
    nr--;
    if ((n == 0 && v->p != 0) || (n != 0))
        nr += ft_pst(s);
    return (nr);
}

int		ft_pp_ver2(t_printf *v, uintmax_t n, int ln, char *s)
{
    int nr;

    nr = 0;
    if (v->f->d && ++nr)
        ft_putchar('0');
    while (nr++ < (v->p - ln))
        ft_putchar('0');
    if ((n == 0 && v->p != 0) || (n != 0))
        nr += ft_pst(s);
    nr--;
    while (nr++ < v->w)
        ft_putchar(' ');
    nr--;
    return (nr);
}