#include "ft_stdio.h"

int		ft_sw4(t_printf *v, va_list ap)
{
    int	n;

    n = 0;
    if (v->s == 's')
        n = ft_psp(v, va_arg(ap, wchar_t*));
    else if (v->s == 'd' || v->s == 'i')
        n = ft_pd(v, (long int)va_arg(ap, uintmax_t));
    else if (v->s == 'c')
        n = ft_pspch(v, va_arg(ap, wchar_t));
    else if (v->s == 'o')
        n = ft_pp(v, (unsigned long int)va_arg(ap, uintmax_t), 8);
    else if (v->s == 'u')
        n = ft_pp(v, (unsigned long int)va_arg(ap, uintmax_t), 10);
    else if (v->s == 'x')
        n = ft_ph(v, (unsigned long int)va_arg(ap, uintmax_t), 0);
    else if (v->s == 'X')
        n = ft_ph(v, (unsigned long int)va_arg(ap, uintmax_t), 1);
    else
        n = ft_sw1(v, ap);
    return (n);
}

int		ft_sw5(t_printf *v, va_list ap)
{
    int	n;

    n = 0;
    if (v->s == 'd' || v->s == 'i')
        n = ft_pd(v, va_arg(ap, long long int));
    else if (v->s == 'o')
        n = ft_pp(v, va_arg(ap, uintmax_t), 8);
    else if (v->s == 'u')
        n = ft_pp(v, va_arg(ap, uintmax_t), 10);
    else if (v->s == 'x')
        n = ft_ph(v, va_arg(ap, uintmax_t), 0);
    else if (v->s == 'X')
        n = ft_ph(v, va_arg(ap, uintmax_t), 1);
    else
        n = ft_sw1(v, ap);
    return (n);
}

int		ft_sw6(t_printf *v, va_list ap)
{
    int	n;

    n = 0;
    if (v->s == 'd' || v->s == 'i')
        n = ft_pd(v, va_arg(ap, intmax_t));
    else if (v->s == 'o')
        n = ft_pp(v, va_arg(ap, uintmax_t), 8);
    else if (v->s == 'u')
        n = ft_pp(v, va_arg(ap, uintmax_t), 10);
    else if (v->s == 'x')
        n = ft_ph(v, va_arg(ap, uintmax_t), 0);
    else if (v->s == 'X')
        n = ft_ph(v, va_arg(ap, uintmax_t), 1);
    else
        n = ft_sw1(v, ap);
    return (n);
}

int		ft_sw7(t_printf *v, va_list ap)
{
    int	n;

    n = 0;
    if (v->s == 'd' || v->s == 'i')
        n = ft_pd(v, va_arg(ap, size_t));
    else if (v->s == 'o')
        n = ft_pp(v, va_arg(ap, size_t), 8);
    else if (v->s == 'u')
        n = ft_pp(v, va_arg(ap, size_t), 10);
    else if (v->s == 'x')
        n = ft_ph(v, va_arg(ap, size_t), 0);
    else if (v->s == 'X')
        n = ft_ph(v, va_arg(ap, size_t), 1);
    else
        n = ft_sw1(v, ap);
    return (n);
}