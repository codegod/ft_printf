/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sw.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/19 17:31:32 by ccuciurc          #+#    #+#             */
/*   Updated: 2017/01/19 18:13:12 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stdio.h"

int ft_sw(t_printf *v, va_list ap)
{
	int n;

	n = 0;
	if (v->s != '\0')
	{
		if (!ft_strcmp("hh", v->l))
			n = ft_sw2(v, ap);
		else if (!ft_strcmp("h", v->l))
			n = ft_sw3(v, ap);
		else if (!ft_strcmp("l", v->l))
			n = ft_sw4(v, ap);
		else if (!ft_strcmp("ll", v->l))
			n = ft_sw5(v, ap);
		else if (!ft_strcmp("j", v->l))
			n = ft_sw6(v, ap);
		else if (!ft_strcmp("z", v->l))
			n = ft_sw7(v, ap);
		else
			n = ft_sw1(v, ap);
	}
    ft_uninit(&v);
	return (n);
}

int		ft_sw1(t_printf *v, va_list ap)
{
	int	n;

	n = 0;
	if (v->s == 's')
		n = ft_prints(v, va_arg(ap, char*));
	else if (v->s == 'd' || v->s == 'i')
		n = ft_pd(v, va_arg(ap, int));
    else if (v->s == 'D')
        n = ft_pd(v, (long int)va_arg(ap, uintmax_t));
	else if (v->s == 'p')
		n = ft_ppt(v, va_arg(ap, uintmax_t));
	else if (v->s == 'c')
		n = ft_pch(v, va_arg(ap, int));
    else if (v->s == 'C')
        n = ft_pspch(v, va_arg(ap, wchar_t));
	else if (v->s == 'S')
		n = ft_psp(v, va_arg(ap, wchar_t*));
	else
		n = ft_sw1c(v, ap);
	return (n);
}

int		ft_sw1c(t_printf *v, va_list ap)
{
	int n;

	n = 0;
	if (v->s == 'o')
		n = ft_pp(v, va_arg(ap, unsigned int), 8);
	else if (v->s == 'O')
		n = ft_pp(v, va_arg(ap, unsigned long long int), 8);
	else if (v->s == 'u')
		n = ft_pp(v, va_arg(ap, unsigned int), 10);
	else if (v->s == 'U')
		n = ft_pp(v, va_arg(ap, unsigned long), 10);
	else if (v->s == 'x')
		n = ft_ph(v, va_arg(ap, unsigned int), 0);
	else if (v->s == 'X')
		n = ft_ph(v, va_arg(ap, unsigned int), 1);
	else
		n = ft_pch(v, v->s);
	return (n);
}

int		ft_sw2(t_printf *v, va_list ap)
{
	int	n;

	n = 0;
	if (v->s == 'd' || v->s == 'i')
		n = ft_pd(v, (signed char)va_arg(ap, int));
	else if (v->s == 'o')
		n = ft_pp(v, (unsigned char)va_arg(ap, uintmax_t), 8);
	else if (v->s == 'u')
		n = ft_pp(v, (unsigned char)va_arg(ap, uintmax_t), 10);
	else if (v->s == 'x')
		n = ft_ph(v, (unsigned char)va_arg(ap, uintmax_t), 0);
	else if (v->s == 'X')
		n = ft_ph(v, (unsigned char)va_arg(ap, uintmax_t), 1);
	else
		n = ft_sw1(v, ap);
	return (n);
}

int		ft_sw3(t_printf *v, va_list ap)
{
	int	n;

	n = 0;
	if (v->s == 'd' || v->s == 'i')
		n = ft_pd(v, (short int)va_arg(ap, int));
	else if (v->s == 'o')
		n = ft_pp(v, (unsigned short int)va_arg(ap, uintmax_t), 8);
	else if (v->s == 'u')
		n = ft_pp(v, (unsigned short int)va_arg(ap, uintmax_t), 10);
	else if (v->s == 'x')
		n = ft_ph(v, (unsigned short int)va_arg(ap, uintmax_t), 0);
	else if (v->s == 'X')
		n = ft_ph(v, (unsigned short int)va_arg(ap, uintmax_t), 1);
	else
		n = ft_sw1(v, ap);
	return (n);
}