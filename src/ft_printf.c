/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 16:19:43 by ccuciurc          #+#    #+#             */
/*   Updated: 2017/01/19 17:38:48 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stdio.h"

#include <stdio.h>

void ft_afpar(t_printf *v)
{
	if (v)
	{
		if (v->f)
		{
			printf("Flag diez: %d\n", v->f->d);
			printf("Flag minus: %d\n", v->f->m);
			printf("Flag plus: %d\n", v->f->p);
			printf("Flag space: %d\n", v->f->s);
			printf("Flag zero: %d\n", v->f->z);
		}
		printf("Width: %d\n", v->w);
		printf("Precision: %d\n", v->p);
		printf("Length: %s\n", v->l);
		printf("Specifier: %c\n", v->s);
	}
}

int			ft_det(char **c, va_list ap)
{
	t_printf	*v;

	ft_pinit(&v);
	while (ft_fl(**c) || ft_wd(**c) || ft_isl(**c) || (**c == '.'))
	{
		ft_det_flags_width(c, ap, &v);
		ft_det_precision(c, ap, &v);
		ft_det_length(c, &v);
	}
	v->s = **c;
	ft_afpar(v);
	return (ft_sw(v, ap));
}

void ft_det_flags_width(char **c, va_list ap, t_printf **v)
{
	while (ft_fl(**c))
	{
		ft_which(&((*v)->f), **c);
		(*c)++;
	}
	if (ft_wd(**c))
	{
		if (**c == '*')
		{
			(*v)->w = va_arg(ap, int);
			(*v)->w < 0 ? ft_which(&((*v)->f), '-') : 0;
			(*v)->w < 0 ? (*v)->w = ft_abs((*v)->w) : 0;
			(*c)++;
		}
		else
		{
			(*v)->w = ft_atoi(*c);
			while (ft_isdigit(**c))
			{
				(*c)++;
			}
		}
	}
}

void 	ft_det_precision(char **c, va_list ap, t_printf **v)
{
	if (**c == '.')
	{
		(*c)++;
		if (**c == '*')
		{
			(*v)->p = va_arg(ap, int);
			(*v)->p < 0 ? (*v)->p = -1: 0;
			(*c)++;
		}
		else
		{
			(*v)->p = ft_atoi(*c);
			while (ft_isdigit(**c))
			{
				(*c)++;
			}
		}
	}
}

void		ft_det_length(char **c, t_printf **v)
{
	int i;

	if (ft_isl(**c) && ft_is_bigger((*v)->l, *c, ft_nrl(*c)))
	{
		free((*v)->l);
		(*v)->l = ft_strnew((size_t)ft_nrl(*c) + 1);
		i = 0;
		while (ft_isl(**c) && (i < ft_nrl(*c - i)))
		{
			(*v)->l[i] = **c;
			i++;
			(*c)++;
		}
	}
	else if (ft_isl(**c))
		(*c)++;
}

int		ft_printf(char *f, ...)
{
	va_list	ap;
	int	n;

	n = 0;
	va_start(ap, f);
	while (*f)
	{
		if (*f != '%')
		{
			n++;
			ft_putchar(*f);
		}
		else
		{
			f++;
			n += ft_det(&f, ap);
		}
		if (*f)
			f++;
	}
	va_end(ap);
	return (n);
}
