/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strings.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 16:20:26 by ccuciurc          #+#    #+#             */
/*   Updated: 2017/01/19 17:43:07 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stdio.h"

int		ft_prints(t_printf *v, char *s)
{
	int		n;
	int		all;

	all = 0;
	n = 0;
	if (s == NULL)
	{
		all = 1;
		s = ft_strdup("(null)");
	}
	if (v->f->m)
	{
		if (v->p != -1)
			n = ft_npst(s, v->p);
		else
			n = ft_pst(s);
		while (n++ < v->w)
			ft_putchar(' ');
		n--;
	}
	else
		n = ft_printsc(v, s);
	if (all)
		free(s);
	return (n);
}

int 	ft_printsc(t_printf *v, char *s)
{
	int		n;
    int     len;

	n = 0;
    len = (int)ft_strlen(s);
	while (n < (v->w - len + (v->p != -1 && v->p <= len ? len - v->p : 0)))
	{
		if (v->f->z)
			ft_putchar('0');
		else
			ft_putchar(' ');
		n++;
	}
	if (v->p != -1)
		n += ft_npst(s, v->p);
	else
		n += ft_pst(s);
	return (n);
}

int		ft_pch(t_printf *v, char c)
{
	int	nr;

	nr = 0;
	if (v->f->m)
	{
		ft_putchar(c);
		while (++nr < v->w)
			ft_putchar(' ');
	}
	else
	{
		while (++nr < v->w)
			if (v->f->z)
				ft_putchar('0');
			else
				ft_putchar(' ');
		ft_putchar(c);
	}
	return (nr);
}