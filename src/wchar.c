/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wchar.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/19 17:50:01 by ccuciurc          #+#    #+#             */
/*   Updated: 2017/01/19 17:53:11 by ccuciurc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stdio.h"

int   ft_putwstr(wchar_t *wstr, size_t len)
{
  size_t i;
  size_t n;

  i = 0;
  while (*wstr)
  {
    n = i;
    if (*wstr < 128)
      i++;
    else if (*wstr < 2047)
      i += 2;
    else if (*wstr < 65535)
      i += 3;
    else
      i += 4;
    if (i <= len)
      ft_putchar_fd(*wstr, 1);
    else
    {
      i = n;
      break;
    }
    wstr++;
  }
  return ((int)i);
}

int   ft_wstrl(wchar_t *wstr, size_t len)
{
  size_t i;
  size_t n;

  i = 0;
  while (*wstr)
  {
    n = i;
    if (*wstr < 128)
      i++;
    else if (*wstr < 2047)
      i += 2;
    else if (*wstr < 65535)
      i += 3;
    else
      i += 4;
    if (i <= len)
      ft_abs(1);
    else
    {
      i = n;
      break;
    }
    wstr++;
  }
  (void)wstr;
  return ((int)i);
}

wchar_t *ft_wstrnew(char *s)
{
  wchar_t *s2;
  int		i;

  i = 0;
  while (s[i])
    i++;
  s2 = (wchar_t*)malloc(sizeof(wchar_t) * (i + 1));
  if (!s2)
    return (NULL);
  i = 0;
  while (s[i])
  {
    s2[i] = s[i];
    i++;
  }
  s2[i] = '\0';
  return (s2);
}

int		ft_wstrlen(wchar_t *wstr)
{
  int i;

  i = 0;
  while (*wstr)
  {
    if (*wstr < 128)
      i++;
    else if (*wstr < 2047)
      i += 2;
    else if (*wstr < 65535)
      i += 3;
    else
      i += 4;
    wstr++;
  }
  return (i);
}