# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ccuciurc <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/28 19:15:07 by ccuciurc          #+#    #+#              #
#    Updated: 2017/01/17 14:59:09 by ccuciurc         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

SRC = main.c bool_func.c ft_printf.c strings.c num.c vrf.c pointer.c wchar.c sw.c struct.c

FLAGS = 

NAME = printf

LI = -I./libft
LL = -L./libft -lft
LFT = ./libft/libft.a

SD = ./src/
ID = ./includes/
OD = ./obj/

OBJ = $(addprefix $(OD), $(SRC:.c=.o))

all: $(NAME)

obj: 
	mkdir -p $(OD)

$(OD)%.o:$(SD)%.c
	$(CC) $(FLAGS) -o $@ -c $< $(LI) -I $(ID)

libft: $(LFT)

$(LFT):
	make -C ./libft

$(NAME): obj libft $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(LL)

clean:
	rm -rf $(OD)
	make fclean -C ./libft

fclean: clean
	rm -rf $(NAME)
	make fclean -C ./libft

re: fclean all
